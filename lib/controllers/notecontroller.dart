import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/note.dart';
import '../views/notepageedit.dart';


class NoteController extends ControllerMVC{
  factory NoteController(){
    if (_this == null){
      _this = NoteController._();
    }
    return _this!;
  }
  static NoteController? _this;
  NoteController._();
  List<Note> notes = [];


  Future loadNotes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      notes = (prefs.getStringList('notes') ?? []).map((note) {
        List<String> noteData = note.split(',');
        return Note(
          title: noteData[0],
          description: noteData[1],
        );
      }).toList();
    });
  }

  Future saveNotes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> savedNotes = notes.map((note) {
      return '${note.title},${note.description}';
    }).toList();
    await prefs.setStringList('notes', savedNotes);
  }

  void addNote(BuildContext context) async {
    Note? result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NoteEditPage()),
    );
    if (result != null) {
      notes.add(result);
      saveNotes();
      refresh();
    }
  }

  void saveNote(BuildContext context, TextEditingController titleController, TextEditingController descriptionController) {
    Note note = Note(
      title: titleController.text,
      description: descriptionController.text,
    );
    Navigator.pop(context, note);
  }

  void editNote(int index, BuildContext context) async {
    Note? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteEditPage(note: notes[index]),
      ),
    );
    if (result != null) {
      setState(() {
        notes[index] = result;
        saveNotes();
      });
    }
  }
  void deleteNote(int index, BuildContext context) async {
    bool confirm = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Удалить заметку?'),
          content: Text('Вы действительно хотите удалить эту заметку?'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: Text('Отмена'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: Text('Удалить'),
            ),
          ],
        );
      },
    );
    if (confirm != null && confirm) {
      setState(() {
        notes.removeAt(index);
        saveNotes();
      });
    }
  }
}