import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/notecontroller.dart';
import '../models/note.dart';


class NoteListPage extends StatefulWidget {
  @override
  _NoteListPageState createState() => _NoteListPageState();
}


class _NoteListPageState extends StateMVC<NoteListPage> {
  late NoteController _con;

  _NoteListPageState() : super(NoteController()) {
    _con = controller as NoteController;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Заметки'),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1.0,
        ),
        itemCount: _con.notes.length,
        itemBuilder: (context, index) {
          String title = _con.notes[index].title;
          String description = _con.notes[index].description;
          return Card(
            child: ListTile(
              title: Text(title),
              subtitle: Text(description),
              onTap: () => _con.editNote(index, this.context),
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () => NoteController().deleteNote(index, this.context),
                ),
              ),
            );
          },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {_con.addNote(this.context);}),
        child: Icon(Icons.add),
      ),
    );
  }
}



