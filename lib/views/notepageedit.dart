import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../controllers/notecontroller.dart';
import '../models/note.dart';




class NoteEditPage extends StatefulWidget {
  final Note? note;
  NoteEditPage({this.note});
  @override
  _NoteEditPageState createState() => _NoteEditPageState();
}

class _NoteEditPageState extends StateMVC<NoteEditPage> {
  late NoteController _con;
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();

  _NoteEditPageState() : super(NoteController()) {
    _con = controller as NoteController;
  }

  @override
  void initState() {
    super.initState();
    if (widget.note != null) {
      titleController.text = widget.note!.title;
      descriptionController.text = widget.note!.description;
    }
  }

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Редактировать заметку'),
        actions: [
          IconButton(
            onPressed: () => _con.saveNote(this.context, titleController, descriptionController),
            icon: Icon(Icons.check),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              TextField(
                controller: titleController,
                decoration: InputDecoration(
                  labelText: 'Заголовок',
                ),
              ),
              SizedBox(height: 16.0),
              TextField(
                controller: descriptionController,
                maxLines: 8,
                decoration: InputDecoration(
                  labelText: 'Текст',
                ),
              ),
              SizedBox(height: 16.0),
            ],
          ),
        ),
      ),
    );
  }
}